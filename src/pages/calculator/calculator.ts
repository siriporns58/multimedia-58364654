import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the CalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})
export class CalculatorPage {
  public Income:number;
  public Expense:number;
  public Cals:number;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.cal(this.Income,this.Expense)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculatorPage');
    this.cal(this.Income,this.Expense)
    console.log(this.Cals);
  }
public cal(Income,Expense){
 
 this.Cals=  this.Income-this.Expense;
 console.log(this.Cals);

}
}
